// Опишіть своїми словами як працює метод forEach.
// Используется для однотипных действий с каждым
// элементом массива без его изменения (массива).

// Як очистити масив?
// const arr = [1, 2, 3, 4];
// arr.length=0;
// or
// arr.splice(0, arr.length);

// Як можна перевірити, що та чи інша змінна є масивом?
// Array.isArray(переменная); Вернет true или false.

const arr = [
  { name: "asd", age: 23 },
  "hello",
  "world",
  23,
  "23",
  null,
  "asdas",
  "hssd12213",
  123,
];

let dataType = "object";

function filterBy(array, dataType) {
  return array.filter((str) => {
    return typeof str !== dataType;
  });
}

const newArr = filterBy(arr, dataType);
console.log(arr);
console.log(newArr);
